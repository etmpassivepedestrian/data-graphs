﻿//Set the dimensions of the canvas of the graph
var margin = { top: 30, right: 30, bottom: 70, left: 85 },
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

// Parse the date / time
var formatTime = d3.time.format("%Y-%m-%d-%H-%M-%S").parse;

//Set the ranges
var x = d3.time.scale().range([0, width]);
var y = d3.scale.linear().range([height, 0]);

//Define the axes
var xAxis = d3.svg.axis().scale(x)
    .orient("bottom").ticks(5);
var yAxis = d3.svg.axis().scale(y)
    .orient("left").ticks(5);

//Define the line
var valueline = d3.svg.line()
    .x(function (d) { return x(d.Time); })
    .y(function (d) { return y(d.Detections); });

//Define the div for the tooltip
var div = d3.select("body")
    .append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

//Adds the svg canvas
var svg = d3.select("body")
  .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

//Load the data from a TSV
d3.tsv("http://ltnodeserver.no-ip.org:80/example.tsv", function (error, data) {
    data.forEach(function (d) {
        d.Time = formatTime(d.Time);
        d.Detections = +d.Detections;
    })
    if (error) throw error;

    //Scale the range of the data
    x.domain(d3.extent(data, function (d) { return d.Time; }));
    y.domain([0, d3.max(data, function (d) { return d.Detections; })]);

    //Nest the entries by symbol
    var dataNest = d3.nest()
        .key(function (d) { return d.Node; })
        .entries(data);

    //Set the colour scale
    var color = d3.scale.category10();

    //Set the spacing for legend
    legendSpace = width / dataNest.length;

    //Loop through each Symbol / key
    dataNest.forEach(function (d,i) {
        //Add line path
        svg.append("path")
            .attr("class", "line")
            .style("stroke", function () { // Add colours dynamically
                return d.color = color(d.key); })
            .attr("id", 'tag'+d.key.replace(/\s+/g, '')) //asign ID
            .attr("d", valueline(d.values));

        //Add the scatterplot
        svg.selectAll("dot")
            .data(d.values)
            .enter().append("circle")
            .style("fill", function () { // add colours dynamically
                return d.color = color(d.key); })
            .attr("r", 5)
            .attr("cx", function (d) { return x(d.Time); })
            .attr("cy", function (d) { return y(d.Detections); })
            .on("mouseover", function (d) {
                div.transition()
                    .duration(200)
                    .style("opacity", .9);
                div.html("Node: " + d.Node + "<br/>" + "Time: " + d.Time + "<br/>" + "Detections: " + d.Detections)
                    .style("left", (d3.event.pageX - 250) + "px")
                    .style("top", (d3.event.pageY) + "px");
            })
            .on("mouseout", function (d) {
                div.transition()
                    .duration(500)
                    .style("opacity", 0);
            });

        //Add the legend
        svg.append("text")
            .attr("x", (legendSpace / 2) + i * legendSpace) // space legend
            .attr("y", height + (margin.bottom / 2) + 20)
            .attr("class", "legend") // style te legend
            .style("fill", function () { // add colours dynamically
                return d.color = color(d.key); })
            .on("click", function () {
                // Determine if current line is visible
                var active = d.active ? false : true,
                    newOpacity = active ? 5 : 3;

                //Hide or show the elements based on the ID
                d3.select('#tag' + d.key.replace(/\s+/g, ''))
                    .transition().duration(100)
                    .style('stroke-width', newOpacity);

                //Update whether or not the elements are active
                d.active = active;
            })
            .text(d.key);
    });

    //Add the x axis
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
      .append("text")
        .attr("dy", "2em")
        .text("Time");

    //Add the y axis
    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
      .append("text")
        .attr("transform", "rotate(-90)")
        .attr("dy", "-2.5em")
        .style("text-anchor", "end")
        .text("Detections");
});